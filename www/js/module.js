(function(exports) {

	var clickedCardId = 0;
	var pairsRemain = 0;
	var dif = 1;
	var baseScore = 200;
	var bonusScore = 200;
	var scores;
	var totalCards;
	var time;
	$(document).on('click','#play', function(){
		var ammount = parseInt($("#fieldAmmount").val().trim());
		if (isNaN(ammount) || (ammount<5) || (ammount > 41))
		{
			return;
		}
		loadGame(ammount);
	});

	$(document).on('click','#newGame', function(){
		var mark = '<div><div class="InputDescr">Количество карт</div><input id="fieldAmmount" value="10" class="InputField" type="text"><br><select id="difficulty">';
		mark+='<option value="3">Легко</option><option value="2">Средне</option><option value="1">Сложно</option></select><br>';
		mark+='<button id="play" style="margin-top:10px">играть</button></div>';
		$("#field").html(mark);
	});

	$(document).on('click','#sendScore', function(){
		var name = $("#nickName").val();
		if (name.length>20)
		{
			name = name.subsctring(0,20);
		}
		$.ajax({
			url:"dbScripts.php",
			type: "POST",
			data: {'action':'insert', 'name':name, 'score':scores},
			responce:'text',
			success: function(data){
				$("#nickName").remove();
				$("#sendScore").remove();
				alert("Результат игры отправлен");
			},
			error:function(data){}
		});
	});

	$(document).on('click','.cardWrapper', function(){
		var t = time - new Date().getTime();
		if ($(this).find(".shirt").css("display")=="none")
		{
			return;
		}
		$(this).find(".shirt").css("display", "none");//открытие карты
		$(this).find(".cardFace").css("display", "block");
		if(clickedCardId==0)
		{
			clickedCardId = $(this).find(".shirt").attr("id");
			return;
		}

		if ($(this).find(".shirt").attr("id")!=-clickedCardId)//если не угадал
		{
			var buf = clickedCardId;
			var buf2 = $(this).find(".shirt").attr("id");
			clickedCardId = 0;
			setTimeout(function(){
				$("#"+buf2).find(".cardFace").css("display", "block");
				$("#"+buf).find(".cardFace").css("display", "block");
				$("#"+buf2).css("display", "block");
				$("#"+buf).css("display", "block");
			},500);
		}
		else//если угадал
		{
			var currScore = parseInt($("#score").text());

			var addScore = baseScore + (20*dif*totalCards-t/1000);
			if (addScore<baseScore)
				addScore = baseScore;

			currScore +=parseInt(addScore);
			$("#score").text(currScore);
			scores = currScore;

			clickedCardId = 0;
			if (--pairsRemain==0)
			{
				endGameShow();
			}
		}
	});

	function endGameShow () {
		$("#field").html("<div style='width:100%; text-align:center;'>Загрузка...</div>");
		$.ajax({
			url:"dbScripts.php",
			type: "POST",
			data: {'action':'read'},
			responce:'json',
			success: function(data){
				data = JSON.parse(data);
				console.log(Object.keys(data));

				res = Object.keys(data).map(function(k) { return data[k] });
				var mark = "<center><div style='width:400px; text-align:center;'><h2>Лучшие результаты</h2>";
				for (var i = 0; i < res.length; i+=2) {
					mark+="<div>"+res[i]+" - "+res[i+1]+"</div>"
				};
				mark+="</div><center><br><h2>Ваш счёт</h2><div><h2>"+scores+"</h2></div><br>";
				mark+='<div>Введите ваше имя<br></div><input maxlength="20" id="nickName" class="InputField" style="width:150px;" type="text"><br>';
				mark+='<button id="sendScore" style="margin-top:10px;width:150px;">Отправить счёт</button><br>';
				mark+='<button id="newGame" style="margin-top:10px;width:150px;">Новая игра</button>';
				$("#field").html(mark);
				
			},
			error:function(data){
				$("#field").html("<div style='width:100%; text-align:center;'>Ошибка: сервер недоступен</div>");
			}
			});
	}

	function loadGame (ammount) {//выборка, перемешивание и расстановка карточек
		totalCards = ammount;
		scores = 0;
		$.ajax({
			url:"array.php",
			type: "POST",
			data: {'count':ammount},
			responce:'json',
			success: function(data){
				data = JSON.parse(data);
				res = Object.keys(data).map(function(k) { return data[k] });

				dif = $("#difficulty").val();
				$("#field").html("");
				var cardsPath = new Array(ammount*2);
				pairsRemain = ammount;

				$("#field").append("<div style='width:100%; padding:10px;'>Очки: <div id='score'>0</div><div>");
				time = new Date().getTime();

				res.forEach(function(item,i,res){
					cardsPath[i] = "images/"+item+".png";//массив адресов картинок для прелоадера
				});

				res.forEach(function(item, i, res){
					$("#field").append("<div class='cardWrapper'><div class='cardFace'><img height='100%' width='100%' src='images/"+Math.abs(item)+".png'></div><div id='"+item+"' class='shirt'></div></div>");
				});
			}
		});
	}

	function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	}

  exports.name = function(number) {
    return names[number];
  };
  exports.number = function(name) {
    return names.indexOf(name);
  };
})(this.pairsGame = {});